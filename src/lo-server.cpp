/**************************************************************************/
/*  lo-server.cpp                                                         */
/**************************************************************************/
/*                         This file is part of:                          */
/*                          godot-osc extension                           */
/*                        https://godotengine.org                         */
/**************************************************************************/
/* Copyright (c) 2014-present Godot Engine contributors (see AUTHORS.md). */
/* Copyright (c) 2007-2014 Juan Linietsky, Ariel Manzur.                  */
/*                                                                        */
/* Permission is hereby granted, free of charge, to any person obtaining  */
/* a copy of this software and associated documentation files (the        */
/* "Software"), to deal in the Software without restriction, including    */
/* without limitation the rights to use, copy, modify, merge, publish,    */
/* distribute, sublicense, and/or sell copies of the Software, and to     */
/* permit persons to whom the Software is furnished to do so, subject to  */
/* the following conditions:                                              */
/*                                                                        */
/* The above copyright notice and this permission notice shall be         */
/* included in all copies or substantial portions of the Software.        */
/*                                                                        */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,        */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF     */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. */
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY   */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,   */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                 */
/**************************************************************************/

#include <algorithm>
#include <iostream>
#include <godot_cpp/variant/utility_functions.hpp>
#include "lo-server.hpp"

namespace gdext::osc {

LoServer::LoServer(const char *port)
    : server_(lo_server_thread_new(port, on_error)) {
  if (nullptr == server_) {
    return;
  }
  lo_server_thread_add_method(server_, nullptr, nullptr, generic_handler, this);
  lo_server_thread_start(server_);
}

LoServer::~LoServer() {
  if (nullptr != server_) {
    lo_server_thread_free(server_);
  }
}

godot::Array LoServer::pop_messages() {
  godot::Array res;
  {
    std::lock_guard<std::mutex> lock(msg_mutex_);
    std::swap(messages_, res);
  }
  return res;
}

void LoServer::on_error(int num, const char *msg, const char *path) {
  godot::UtilityFunctions::printerr("liblo server error ", num, ":", msg, " (path is ", path, ")");
}

int LoServer::generic_handler(const char *path, const char *types,
                              lo_arg **argv, int argc, lo_message data,
                              void *user_data) {
  using namespace godot;

  // get back the LoServer instance
  auto *context = static_cast<LoServer *>(user_data);
  
  // initialise Godot Arrays
  Array msg; // ["path", [..values..]]
  Array values;
  msg.push_front(Variant(path));
  msg.push_back(Variant(values));
  
  // iterate over the message and feed to Godot value array
  for (int i = 0; i < argc; i++) {
    if (types[i] == lo_type::LO_INT32) {
      values.push_back(Variant(argv[i]->i32));
    } else if (types[i] == lo_type::LO_FLOAT) {
      values.push_back(Variant(argv[i]->f32));
    } else if (types[i] == lo_type::LO_STRING) {
      values.push_back(Variant(&argv[i]->s));
    } else if (types[i] == lo_type::LO_BLOB) {
      PackedByteArray byte_array;
      byte_array.resize(argv[i]->blob.size);
      std::memcpy(byte_array.ptrw(), &argv[i]->blob.data, argv[i]->blob.size);
      values.push_back(Variant(&byte_array));
    } else if (types[i] == lo_type::LO_INT64) {
      values.push_back(Variant(argv[i]->i64));
    } else if (types[i] == lo_type::LO_TIMETAG) {
      godot::UtilityFunctions::printerr("OSCReceiver: format LO_TIMETAG not supported");
    } else if (types[i] == lo_type::LO_DOUBLE) {
      values.push_back(Variant(argv[i]->f64));
    } else if (types[i] == lo_type::LO_SYMBOL) {
      values.push_back(Variant(&argv[i]->S));
    } else if (types[i] == lo_type::LO_CHAR) {
      values.push_back(Variant(&argv[i]->c));
    } else if (types[i] == lo_type::LO_MIDI) {
      PackedByteArray byte_array;
      byte_array.resize(4);
      byte_array[0] = argv[i]->m[0];
      byte_array[1] = argv[i]->m[1];
      byte_array[2] = argv[i]->m[2];
      byte_array[3] = argv[i]->m[3];
      values.push_back(Variant(&byte_array));
    } else if (types[i] == lo_type::LO_TRUE) {
      values.push_back(Variant(true));
    } else if (types[i] == lo_type::LO_FALSE) {
      values.push_back(Variant(false));
    } else if (types[i] == lo_type::LO_NIL) {
      values.push_back(Variant());
    } else if (types[i] == lo_type::LO_INFINITUM) {
      godot::UtilityFunctions::printerr("OSCReceiver: format LO_INFINITUM not supported");
    }
  }  // end iterate over osc message values

  std::lock_guard<std::mutex> lock(context->msg_mutex_);
  context->messages_.push_back(msg);
  
  return 1;
}

} // namespace gdext::osc
