/**************************************************************************/
/*  lo-server.hpp                                                         */
/**************************************************************************/
/*                         This file is part of:                          */
/*                          godot-osc extension                           */
/*                        https://godotengine.org                         */
/**************************************************************************/
/* Copyright (c) 2014-present Godot Engine contributors (see AUTHORS.md). */
/* Copyright (c) 2007-2014 Juan Linietsky, Ariel Manzur.                  */
/*                                                                        */
/* Permission is hereby granted, free of charge, to any person obtaining  */
/* a copy of this software and associated documentation files (the        */
/* "Software"), to deal in the Software without restriction, including    */
/* without limitation the rights to use, copy, modify, merge, publish,    */
/* distribute, sublicense, and/or sell copies of the Software, and to     */
/* permit persons to whom the Software is furnished to do so, subject to  */
/* the following conditions:                                              */
/*                                                                        */
/* The above copyright notice and this permission notice shall be         */
/* included in all copies or substantial portions of the Software.        */
/*                                                                        */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,        */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF     */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. */
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY   */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,   */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                 */
/**************************************************************************/


#include <lo/lo.h>

#ifndef LO_SERVER_HPP
#define LO_SERVER_HPP

/**
 * The LoServer class wraps lo_server_thread creation and deletion
 * into constructor and destructor of a C++ class.
 */

#include <godot_cpp/variant/variant.hpp>
#include <lo/lo.h>
#include <mutex>

namespace gdext::osc {

class LoServer {
  
 public:
  LoServer(const char *port);
  LoServer() = delete;
  ~LoServer();

  godot::Array pop_messages();
  
 private:
  lo_server_thread server_{nullptr};
  godot::Array messages_{};
  std::mutex msg_mutex_{};
  
  static void on_error(int num, const char *msg, const char *path);
  static int generic_handler(const char *path, const char *types, lo_arg **argv,
                             int argc, lo_message data, void *user_data);
};

} // namespace gdext::osc

#endif // LO_SERVER_HPP
