# This script is attached to an "OSCSender" node
extends OSCSender

# Following variable internal values will be sent through 
# OSC at each call to "_process"
var cube : Node3D # a MeshInstance3D in the scene.
var packed_array : PackedVector3Array # for computing values to send

func _ready():
	# get a reference to the cube
	cube = get_node("/root/root/cube")
	# create a new PackedVector3Array
	packed_array = PackedVector3Array()
	# Add some Vector3s to the array
	packed_array.push_back(Vector3(1, 2, 3))
	packed_array.push_back(Vector3(4, 5, 6))


func _process(delta):
	# cube rotation in order to get variation in cube rotation
	cube.rotate_y(delta * 1)
	# change value position in the packed_array
	packed_array[0] = packed_array[0].inverse()
	# send cube name (string) and rotation (3 float values)
	send("/root/cube", [cube.name, cube.rotation])
	# send the packed array content (6 float values)
	send("/root/vectarray", [packed_array])
	
