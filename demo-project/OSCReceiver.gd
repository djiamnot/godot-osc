# This script is attached to an "OSCReceiver" node
extends OSCReceiver

# cube and env are nodes in the scene
var cube : MeshInstance3D
var env : WorldEnvironment

func _ready():
	# get reference to the cube and env nodes
	cube = get_node("/root/root/cube")
	env = get_node("/root/root/WorldEnvironment") 

func _process(delta):
	# get the messages received since the last call to pop_messages()
	var messages = pop_messages()
	# iterate over the received message
	for msg in messages:
		var key = msg[0] # a string with the OSC path
		var values = msg[1] # an array with the OSC values
		if key == "/cube/rotate":
			# apply rotation with expected values "(float, float, float, float)"
			cube.rotate((delta * Vector3(values[0], values[1], values[2])).normalized(), values[3])
		elif key == "/cube/col":
			# change cube RGBA color, with expected values (float, float, float, float)
			cube.get_surface_override_material(0).albedo_color = Color(values[0], values[1], values[2], values[3])
		elif key == "/cube/visible":
			# change cube visibility with a bool value
			cube.visible = values[0]
		elif key == "/bg/col":
			# change horizon RGBA color, with expected values (float, float, float, float)
			env.environment.sky.sky_material.sky_horizon_color = Color(values[0], values[1], values[2], values[3])
		else:
			# print unhandled keys
			print(key + "?")
